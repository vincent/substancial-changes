// Adapted from https://github.com/christophertrml/rs-natural

use std::cmp;

/// Compute the Levenshtein distance between two texts
///
/// ported from [http://hetland.org/coding/python/levenshtein.py](http://hetland.org/coding/python/levenshtein.py)
///
/// [https://en.wikipedia.org/wiki/Levenshtein_distance](https://en.wikipedia.org/wiki/Levenshtein_distance)
///
/// # Arguments
///
/// * `str1` - The first text
/// * `str2` - The second text
///
pub fn levenshtein_distance(str1: &str, str2: &str) -> usize {
    let n = str1.chars().count();
    let m = str2.chars().count();

    let mut column: Vec<usize> = (0..n + 1).collect();
    // TODO this probaly needs to use graphemes
    let a_vec: Vec<char> = str1.chars().collect();
    let b_vec: Vec<char> = str2.chars().collect();
    for i in 1..m + 1 {
        let previous = column;
        column = vec![0; n + 1];
        column[0] = i;
        for j in 1..n + 1 {
            let add = previous[j] + 1;
            let delete = column[j - 1] + 1;
            let mut change = previous[j - 1];
            if a_vec[j - 1] != b_vec[i - 1] {
                change += 1
            }
            column[j] = min3(add, delete, change);
        }
    }
    column[n]
}

fn min3<T: Ord>(a: T, b: T, c: T) -> T {
    cmp::min(a, cmp::min(b, c))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn levenshtein_distance_test_simple() {
        assert_eq!(levenshtein_distance("kitten", "kitty"), 2);
    }

    #[test]
    fn levenshtein_distance_test_equal() {
        assert_eq!(levenshtein_distance("kitten", "kitten"), 0);
    }
}
