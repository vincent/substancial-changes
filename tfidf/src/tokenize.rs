// Adapted from https://github.com/christophertrml/rs-natural

/// Solit the text
///
/// # Arguments
///
/// * `text` - The text to be tokenized
///
pub fn tokenize(text: &str) -> Vec<&str> {
    text.split(Splitter::is_match)
        .filter(|s| !s.is_empty())
        .collect()
}

struct Splitter;

impl Splitter {
    fn is_match(c: char) -> bool {
        match c {
            ' ' | ',' | '.' | '!' | '?' | ';' | '\'' | '"' | ':' | '\t' | '\n' | '(' | ')'
            | '-' => true,
            _ => false,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tokenize_word() {
        assert_eq!(vec!["hello", "world"], tokenize("hello world"))
    }

    #[test]
    fn tokenize_word_and_punc() {
        assert_eq!(
            vec!["hello", "world", "test"],
            tokenize("hello world! test")
        )
    }
}
