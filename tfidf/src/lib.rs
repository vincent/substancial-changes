// Adapted from https://github.com/christophertrml/rs-natural

use rust_stemmers::{Algorithm, Stemmer};
use std::collections::{HashMap, HashSet};
mod tokenize;
use tokenize::tokenize;

#[cfg(feature = "serde_support")]
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Default)]
#[cfg_attr(feature = "serde_support", derive(Serialize, Deserialize))]
pub struct TfIdf {
    // token counts for inverse document frequency
    doc_freqs: HashMap<String, usize>,

    // total count of documents inserted. for IDF
    doc_count: usize,
}

impl TfIdf {
    pub fn new() -> TfIdf {
        TfIdf {
            doc_freqs: HashMap::new(),
            doc_count: 0,
        }
    }

    /// Add documents to test frequency against
    ///
    /// # Arguments
    ///
    /// * `corpus` - The text to be added
    ///
    pub fn add(&mut self, corpus: &str) {
        let tokens = self.get_tokenized_and_stemmed(corpus);

        let mut seen = HashSet::new();

        tokens.iter().for_each(|token| {
            if !seen.contains(&token) {
                seen.insert(token);
                self.doc_freqs
                    .entry(token.to_string())
                    .and_modify(|e| *e += 1)
                    .or_insert(1);
            }
        });

        self.doc_count += 1;
    }

    /// Return the frequency of the term in the document
    ///
    /// # Arguments
    ///
    /// * `term` - The term we want to get the frequency
    /// * `document` - the document when we look for the term
    ///
    fn tf(&self, term: &str, document: &str) -> f32 {
        let tokenized_doc = self.get_tokenized_and_stemmed(document);
        let mut freq: u16 = 0;
        for word in tokenized_doc.iter() {
            if word == term {
                freq += 1u16;
            }
        }

        freq as f32 / tokenized_doc.len() as f32
    }

    /// Calculate inverse document frequency for one term
    /// # Arguments
    ///
    /// * `term` - The term we want to get the IDF
    ///
    fn idf(&self, term: &str) -> f32 {
        let doc_freq = match self.doc_freqs.get(term) {
            Some(freq) => *freq as f32,
            None => 0.0f32,
        };

        let ratio = self.doc_count as f32 / 1.0f32 + doc_freq;

        ratio.ln()
    }

    /// Calculate tf-idf for one term
    /// # Arguments
    ///
    /// * `term` - The term we want to get the IDF
    /// * `document` - The document where the term appears
    ///
    fn tf_idf(&self, term: &str, document: &str) -> f32 {
        let tf = self.tf(term, document);
        let idf = self.idf(term);

        tf * idf
    }

    /// Get tf-idf the average TF-IDF of terms in a document
    /// # Arguments
    ///
    /// * `terms` - The terms we want to get the IDF
    /// * `document` - The document where the terms appear
    ///
    pub fn get(&self, tokens: Vec<&String>, document: &str) -> f32 {
        let score: f32 = tokens
            .iter()
            .map(|x| self.tf_idf(x, document))
            .fold(0.0f32, |acc, x| acc + x); // add together to later divide by token length to get an average

        // average the scores
        score / tokens.len() as f32
    }

    /// Stem and tokenize
    /// # Arguments
    ///
    /// * `text` - The terms we want to get the IDF
    ///
    pub fn get_tokenized_and_stemmed<'a>(&self, text: &'a str) -> Vec<String> {
        let en_stemmer = Stemmer::create(Algorithm::English);
        tokenize(text)
            .into_iter()
            .map(|text| en_stemmer.stem(text).into_owned())
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn doc_count() {
        let mut tf_idf = TfIdf::new();
        tf_idf.add("this document is about rust.");
        tf_idf.add("this document is about erlang.");
        tf_idf.add("this document is about erlang and rust.");
        tf_idf.add("this document is about rust. it has rust examples");

        assert_eq!(tf_idf.doc_count, 4);
    }

    #[test]
    fn tf() {
        let tf_idf = TfIdf::new();

        assert_eq!(
            tf_idf.tf("document", "this document is about rusti"),
            0.2f32
        );
    }

    #[test]
    fn idf_count() {
        let mut tf_idf = TfIdf::new();
        tf_idf.add("this document is about rust.");
        tf_idf.add("this document is about erlang.");
        tf_idf.add("this document is about erlang and rust.");
        tf_idf.add("this document is about rust. it has rust examples");

        assert_eq!(*tf_idf.doc_freqs.get("document").unwrap(), 4);
    }

    #[test]
    #[should_panic]
    fn idf_count_dont_exist() {
        let mut tf_idf = TfIdf::new();
        tf_idf.add("this document is about rust.");

        tf_idf.doc_freqs.get("does_not_exist").unwrap();
    }
}
