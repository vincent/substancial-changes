use clap::{App, Arg};
use std::collections::HashSet;
use std::iter::FromIterator;
use tfidf::TfIdf;

fn file_cb(
    repo: &git2::Repository,
    tf_idf: &mut tfidf::TfIdf,
    diff_delta: git2::DiffDelta,
    commit: String,
) -> (String, String, f32) {
    // Get the content of each side of the diff, without html tags
    let old_content = get_file_content(&repo, diff_delta.old_file().id());
    let new_content = get_file_content(&repo, diff_delta.new_file().id());
    let distance = levenshtein::levenshtein_distance(&old_content, &new_content);

    // Compute TF-IDF on the changed words
    let old_set: HashSet<String> =
        HashSet::from_iter(tf_idf.get_tokenized_and_stemmed(&old_content));
    let new_set: HashSet<String> =
        HashSet::from_iter(tf_idf.get_tokenized_and_stemmed(&new_content));
    let diff_words = old_set
        .symmetric_difference(&new_set)
        .into_iter()
        .collect::<Vec<&String>>();
    let mut tf_idf_value: f32 = 1.0;
    // A changeset may not have word changes
    if diff_words.len() > 0 {
        tf_idf_value = tf_idf.get(
            diff_words,
            [old_content.as_str(), new_content.as_str()]
                .join(" ")
                .as_str(),
        );
    }
    return (
        commit,
        String::from(diff_delta.new_file().path().unwrap().to_str().unwrap()),
        (10.0 * tf_idf_value + distance as f32),
    );
}

fn get_file_content(repo: &git2::Repository, oid: git2::Oid) -> String {
    let blob = repo.find_blob(oid).unwrap();
    let file = String::from_utf8_lossy(blob.content());
    get_raw_text(&file)
}

/// Strip HTML from the file content and return a somewhat cleaner version of it
/// # Arguments
///
/// * `html` - The htmml as a string
///
fn get_raw_text(html: &str) -> String {
    let parsed_html = scraper::Html::parse_document(&html);
    let selector = scraper::Selector::parse("body").unwrap();
    let text_selectors = parsed_html.select(&selector).next().unwrap();
    let re = regex::Regex::new(r"[\n(),]").unwrap();
    re.replace_all(&text_selectors.text().collect::<String>(), "")
        .to_string()
}

/// Fill the IDF cache
/// Get the raw contents of a bunch of documents an add them to the cache
/// # Arguments
///
/// * `tf_idf` - The TfIdf struct
/// * `repo_path` - Path to the fsfe website repo . This is used to get files to feed to IDF
///
fn add_docs(tf_idf: &mut tfidf::TfIdf, repo_path: &str) {
    for file in glob::glob(format!("{}/news/20[12][07-9]/news-*.en.xhtml", repo_path).as_ref())
        .expect("Failed to read glob pattern")
    {
        let file_content = get_raw_text(&std::fs::read_to_string(file.unwrap()).unwrap());
        tf_idf.add(&file_content);
    }
}

fn main() {
    let args = App::new("rank-changes")
        .version(env!("CARGO_PKG_VERSION"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .arg(
            Arg::with_name("repo")
                .long("repo")
                .help("Path to fsfe-website repo")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("commit")
                .long("commit")
                .help("commit id")
                .required(true)
                .takes_value(true),
        )
        .get_matches();

    let mut tf_idf = TfIdf::new();

    let repo_path = args.value_of("repo").unwrap();
    // Add documents to the IDF cache
    add_docs(&mut tf_idf, repo_path);
    let mut diffopts = git2::DiffOptions::new();
    let repo = match git2::Repository::open(repo_path) {
        Ok(repo) => repo,
        Err(e) => panic!("failed to open: {}", e),
    };
    let commit_id = args.value_of("commit").unwrap();
    let commit = repo
        .revparse_single(commit_id)
        .unwrap()
        .peel_to_commit()
        .unwrap();
    let parent = commit.parent(0).unwrap().tree();

    let diff = repo
        .diff_tree_to_tree(
            Some(parent.as_ref().unwrap()),
            Some(&commit.tree().unwrap()),
            Some(&mut diffopts),
        )
        .unwrap();

    let scores = diff
        .deltas()
        // Make sure we care only about modified EN documents
        .filter(|delta| {
            delta
                .new_file()
                .path()
                .unwrap()
                .to_str()
                .unwrap()
                .contains("en.xhtml")
                && delta.status() == git2::Delta::Modified
        })
        .map(&mut |delta| file_cb(&repo, &mut tf_idf, delta, String::from(commit_id)))
        .collect::<Vec<(String, String, f32)>>();
    println!("{:?}", serde_json::to_string(&scores).unwrap())
}
