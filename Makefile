SHELL := /bin/bash
compile:
	cargo build && cargo build --release
score:
# Run the rank-change thing for the last 1000 commits on the website
	cd ../fsfe-website && git log -1000 --pretty="%H" | parallel  -j 3 -I{} ~/projects/substancial-changes/target/release/rank-change --repo ~/projects/fsfe-website/ --commit {} > ../substancial-changes/out
analyze:
# Plot the distribution
	python3 analyze-dist.py
