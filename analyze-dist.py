import json
import seaborn as sns

import matplotlib.pyplot as plt

scores = []

with open("out", "r") as fh:
    for line in fh:
        # Remove first and last character of the json b/c it's surounded by
        # quotes
        line = line[1:-2].replace("\\", "")
        data = json.loads(line)
        if data == []:
            continue
        # Remove outliers. 350 is arbitrary
        scores.extend(
            [change for change in list(filter(lambda score: score[2] < 350, data))]
        )

print("Number of changes: {}".format(len(scores)))

# Plot the distribution
ax = sns.distplot([score[2] for score in scores], kde=False, rug=True)
ax.set_ylabel("count")
ax.set_title("Distibution of the English files change score")
plt.savefig("score.png")

# Print high scores
print([score for score in scores if score[2] > 150])
